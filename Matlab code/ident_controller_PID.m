clear; close all;
%ampe=3;
%path=strcat('/Users/humasoft/Documents/GitHub/Soft-Arm/graphs/Vel/Identification/ID_testP'+string(ampe)+'_interval16.csv');

path=strcat('/Users/humasoft/Documents/GitHub/Soft-Arm/graphs/Vel/Identification/ID_squ_testP3_interval16.csv');%_interval16.csv');

re=csvread(path);
arr=re;
to_zero=0;
freq=50; %Hz
period=1/freq; %sec
test=length(arr); % Numero de datos por test
time=period:period:test/freq;


% Preallocating for speed
alpha= zeros(1, test); beta= zeros(1, test);
pitch= zeros(1, test); roll= zeros(1, test); yaw= zeros(1, test);
cs1= zeros(1, test); cs2= zeros(1, test); cs3= zeros(1, test);
vel1= zeros(1, test); vel2= zeros(1, test); vel3= zeros(1, test);
amp1= zeros(1, test); amp2= zeros(1, test); amp3= zeros(1, test);

for i=1:test %Se asigna el valor de los datos a las variables
    alpha(i)=arr(i,1);
    beta(i)=arr(i,2);
    roll(i)=arr(i,3);
    pitch(i)=arr(i,4);
    yaw(i)=arr(i,5);
    cs1(i)=arr(i,6);
    cs2(i)=arr(i,7);
    cs3(i)=arr(i,8);
    vel1(i)=arr(i,9);
    vel2(i)=arr(i,10);
    vel3(i)=arr(i,11);
    amp1(i)=arr(i,12);
    amp2(i)=arr(i,13);
    amp3(i)=arr(i,14);
end

if(to_zero)
    pitch=pitch-pitch(1);
    yaw=yaw-yaw(1);
    roll=roll-roll(1);
end


% Identificación pitch
dp=0; % Delay del periodo
np=2; % Numero de polos
no=0; % Numero de zeros

part=400:700;

%(1:800)
idset1=iddata(pitch(part)'*180/pi,alpha(part)',period);
tf1pitch=tfest(idset1,np,no,dp*period);
zpk(tf1pitch)

pidTuner(tf1pitch)


%{

np=[pitch(1:106) pitch(307:506) pitch(307:800)];
np=np-np(1);
tim=(1:800)/50;
figure(1);

subplot(2,1,1);

plot(tim,np*180/pi);grid on; hold on;
title('Pitch indentification')
    xlabel('Time (sec)') 
    ylabel('Angle (degree)') 
plot(tim,y)
    %ylim([-0.1 0.8])
legend({'Output: Real pitch data','Output: Identification'},'Location','best');



subplot(2,1,2);
plot(tim,alpha(1:800));grid on; 
legend('Input: Alpha (angular velocity)','Location','best');
    xlabel('Time (sec)') 
    ylabel('Velocity (rad/sec)') 
    ylim([-4 4])

figure(2);
plot(alpha)


G=tf1pitch;

Next=0;
N_interval=8;
amp=-3;
i=1;
for t=0:0.02:16

    if (t>=Next)

        Next=Next+2;
        amp=amp*-1;

    end
    ang(i)=amp;
    i=i+1;
end
plot(ang)

tim=(1:800)/50;
y = lsim(G, ang(1:800), tim);


figure(1);

%subplot(2,1,1);
yyaxis left
plot(tim,np);grid on; hold on;
title('Pitch indentification')
    xlabel('Time (sec)') 
    ylabel('Angle (rad)') 
plot(tim,y,'r')
%subplot(2,1,2);
yyaxis right
plot(tim,alpha(1:800));grid on; 
    xlabel('Time (sec)') 
    ylabel('Velocity (rad/sec)') 
    ylim([-3.5 3.5])
    ylim([-4 4])
legend({'Output: Real pitch data','Output: Identification','Input: Alpha (angular velocity)'},'Location','best');



%}
% Identificación pitch
dp=0; % Delay del periodo
np=2; % Numero de polos
no=0; % Numero de zeros

part=400:700;

%(1:800)
idset1=iddata(pitch(part)'*180/pi,alpha(part)',period);
tf1pitch=tfest(idset1,np,no,dp*period);
zpk(tf1pitch)

pidTuner(tf1pitch)



%{
%tfyaw=tf([-372.2016477613],[1 43.517910379706560 0.263018549483251]);
clear; close all;
%ampe=3;
%path=strcat('/Users/humasoft/Documents/GitHub/Soft-Arm/graphs/Vel/Identification/ID_testP'+string(ampe)+'_interval16.csv');

path=strcat('/Users/humasoft/Documents/GitHub/Soft-Arm/graphs/Vel/Identification/ID_sqr_testY3_interval16.csv');%_interval16.csv');

re=csvread(path);
arr=re;
to_zero=0;
freq=50; %Hz
period=1/freq; %sec
test=length(arr); % Numero de datos por test
time=period:period:test/freq;


% Preallocating for speed
alpha= zeros(1, test); beta= zeros(1, test);
pitch= zeros(1, test); roll= zeros(1, test); yaw= zeros(1, test);
cs1= zeros(1, test); cs2= zeros(1, test); cs3= zeros(1, test);
vel1= zeros(1, test); vel2= zeros(1, test); vel3= zeros(1, test);
amp1= zeros(1, test); amp2= zeros(1, test); amp3= zeros(1, test);

for i=1:test %Se asigna el valor de los datos a las variables
    alpha(i)=arr(i,1);
    beta(i)=arr(i,2);
    roll(i)=arr(i,3);
    pitch(i)=arr(i,4);
    yaw(i)=arr(i,5);
    cs1(i)=arr(i,6);
    cs2(i)=arr(i,7);
    cs3(i)=arr(i,8);
    pos1(i)=arr(i,9);
    pos2(i)=arr(i,10);
    pos3(i)=arr(i,11);
    vel1(i)=arr(i,12);
    vel2(i)=arr(i,13);
    vel3(i)=arr(i,14);
    amp1(i)=arr(i,15);
    amp2(i)=arr(i,16);
    amp3(i)=arr(i,17);
end

if(to_zero)
    pitch=pitch-pitch(1);
    yaw=yaw-yaw(1);
    roll=roll-roll(1);
end

figure(1);
plot(yaw*180/pi)
figure(2);
plot(beta)

% Identificación pitch
dp=0; % Delay del periodo
np=2; % Numero de polos
no=0; % Numero de zeros

part=400:700;

%(1:800)
idset1=iddata(yaw(part)'*180/pi,beta(part)', period);
tf1=tfest(idset1,np,no,dp*period);
zpk(tf1)



figure; hold on;
plot(vel2)
plot(cs2)
tfyaw=tf1;
pidTuner(tfyaw)
%tfyaw=tf([-372.2016477613],[1 43.517910379706560 0.263018549483251]);

%}
