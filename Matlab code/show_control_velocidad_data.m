function [pitch,yaw,time] = show_control_velocidad_data(i_alpha,i_beta,s_type,s_control,s_masa,freq)
%{
i_alpha=0;
i_beta=0;
s_type="FOC"; % 'PI';
s_control="5_60";
freq=50; %Hz
s_masa="500";
%}

s_alpha = string(i_alpha);
s_beta = string(i_beta);

path=strcat('/Users/humasoft/Documents/GitHub/Soft-Arm/graphs/Vel/Control/'+s_type+'/'+s_control+'/Control_Masa_'+s_masa+'_P'+s_alpha+'_Y'+s_beta+'.csv');

re=csvread(path);
arr=re;

period=1/freq; %sec
test=length(arr); % Numero de datos por test
time=period:period:test/freq;


% Preallocating for speed
alpha= zeros(1, test); beta= zeros(1, test);
cs1= zeros(1, test); cs2= zeros(1, test);
pitch= zeros(1, test); roll= zeros(1, test); yaw= zeros(1, test);
v_lengths1= zeros(1, test); v_lengths2= zeros(1, test); v_lengths3= zeros(1, test);
pos1= zeros(1, test); pos2= zeros(1, test); pos3= zeros(1, test);
vel1= zeros(1, test); vel2= zeros(1, test); vel3= zeros(1, test);
amp1= zeros(1, test); amp2= zeros(1, test); amp3= zeros(1, test);

for i=1:test %Se asigna el valor de los datos a las variables
    alpha(i)=arr(i,1);
    beta(i)=arr(i,2);
    cs1(i)=arr(i,3);
    cs2(i)=arr(i,4);
    roll(i)=arr(i,5);
    pitch(i)=arr(i,6);
    yaw(i)=arr(i,7);
    v_lengths1(i)=arr(i,8);
    v_lengths2(i)=arr(i,9);
    v_lengths3(i)=arr(i,10);
    pos1(i)=arr(i,11);
    pos2(i)=arr(i,12);
    pos3(i)=arr(i,13);
    vel1(i)=arr(i,14);
    vel2(i)=arr(i,15);
    vel3(i)=arr(i,16);
    amp1(i)=arr(i,17);
    amp2(i)=arr(i,18);
    amp3(i)=arr(i,19);
end

end