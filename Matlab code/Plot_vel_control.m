% PLOT graficas control
%close all
%% SLOW

freq=50; %Hz
i_alpha=40;
i_beta=0;
s_control="1p5_60";


figure; hold on;
s_type="FOC"; % 'PI';
s_masa="0";
read_control_velocidad_data(i_alpha,i_beta,s_type,s_control,s_masa,freq);
s_masa="500";
read_control_velocidad_data(i_alpha,i_beta,s_type,s_control,s_masa,freq);

figure; hold on;
s_type="PI";
s_masa="0";
read_control_velocidad_data(i_alpha,i_beta,s_type,s_control,s_masa,freq);
s_masa="500";
read_control_velocidad_data(i_alpha,i_beta,s_type,s_control,s_masa,freq);

%% SLOW From 0
% PLOT graficas control
%close all

freq=50; %Hz
i_alpha=40;
i_beta=0;
s_control="1p5_60";


figure; hold on;
s_type="FOC";
s_masa="0";
read_control_velocidad_data(i_alpha,i_beta,s_type,s_control,s_masa,freq);
s_masa="500-0";
read_control_velocidad_data(i_alpha,i_beta,s_type,s_control,s_masa,freq);

figure; hold on;
s_type="PI";
s_masa="0";
read_control_velocidad_data(i_alpha,i_beta,s_type,s_control,s_masa,freq);
s_masa="500-0";
read_control_velocidad_data(i_alpha,i_beta,s_type,s_control,s_masa,freq);


%% FAST CONTROL P=0

freq=50; %Hz
i_alpha=0;
i_beta=0;
s_control="5_60";

figure; hold on;
s_type="FOC"; % 'PI';
s_masa="0";
read_control_velocidad_data(i_alpha,i_beta,s_type,s_control,s_masa,freq);
s_masa="500";
read_control_velocidad_data(i_alpha,i_beta,s_type,s_control,s_masa,freq);

figure; hold on;
s_type="PI";
s_masa="0";
read_control_velocidad_data(i_alpha,i_beta,s_type,s_control,s_masa,freq);
s_masa="500";
read_control_velocidad_data(i_alpha,i_beta,s_type,s_control,s_masa,freq);

%% FAST CONTROL P=40

freq=50; %Hz
i_alpha=40;
i_beta=0;
s_control="5_60";

figure; hold on;
s_type="FOC";
s_masa="0";
read_control_velocidad_data(i_alpha,i_beta,s_type,s_control,s_masa,freq);
s_masa="500";
read_control_velocidad_data(i_alpha,i_beta,s_type,s_control,s_masa,freq);

figure; hold on;
s_type="PI";
s_masa="0";
read_control_velocidad_data(i_alpha,i_beta,s_type,s_control,s_masa,freq);
s_masa="500";
read_control_velocidad_data(i_alpha,i_beta,s_type,s_control,s_masa,freq);

%% FAST CONTROL P=40 From 0
freq=50; %Hz
i_alpha=40;
i_beta=0;
s_control="5_60";


figure; hold on;
s_type="FOC"; 
s_masa="0";
read_control_velocidad_data(i_alpha,i_beta,s_type,s_control,s_masa,freq);
s_masa="500-0";
read_control_velocidad_data(i_alpha,i_beta,s_type,s_control,s_masa,freq);


figure; hold on;
s_type="PI"; 
s_masa="0";
read_control_velocidad_data(i_alpha,i_beta,s_type,s_control,s_masa,freq);
s_masa="500-0";
read_control_velocidad_data(i_alpha,i_beta,s_type,s_control,s_masa,freq);

