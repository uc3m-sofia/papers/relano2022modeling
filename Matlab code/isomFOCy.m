clear;s=tf('s');
close all;
%This file tunes a fractional controller based on the following:
%%%%%%%%%%%% controller specification
wsp=5; %new crossover frequency
pm=60; %new phase margin
dts=0.02;
swsp=string(wsp);
spm=string(pm);
 
% (2.5884)/(s^2 + 2.013s +3.178)
delay=0.0;

%        -372.2
%   ---------------------
%   s^2 + 43.52 s + 0.263

% G=exp(-delay*s)*372.2/(s^2 + 43.52 * s + 0.263)
G=exp(-delay*s)*372.2/( (s+43.51)*(s+0.006044) );
systf=tf(G);


ol=minreal( systf);%open loop
fig=figure;
bode(systf,{wsp/100 wsp*100});grid on;
%saveas(fig,'PhiSysBode_Y'+swsp+'_'+spm,'epsc');
%saveas(fig,'PhiSysBode_Y'+swsp+'_'+spm,'jpg');


%jw array
r0=2;%radius (decades) and center of plots
N = 1000;%precission bigger better
Nm=round(N/2);
w=logspace(-r0+log10(wsp),r0+log10(wsp),(N));
jw = 1i*w;
sys = exp(-delay*jw).*polyval(ol.Numerator{1},jw)./polyval(ol.Denominator{1},jw);


%find system phase and slope
dm=1; %width of positions to include in slope calculation
ps=angle(sys(Nm))*180/pi;
m= - ( angle(sys(Nm+dm))-angle(sys(Nm-dm)) ) / ( log10(w(Nm+dm))-log10(w(Nm-dm)) );


%find required controller phi
phi=-180+pm-ps; %phase required at new frequency
if (ps>0)
ps=ps-360;
phi=-180+pm-ps; %phase required at new frequency
end

disp(phi)


tgp=tan(phi*pi/180);

%find exponent
ed=-0.01:-0.01:-2;
%ed=-ed;
ms=zeros(size(ed));
for i=1:size(ed,2)
    a=ed(i)*pi/2;
    %m1=-(log(10)*cos(a)*ed*tgp^2-log(10)*sin(a)*ed*tgp)/(sin(a)*tgp^2+sin(a))
    m1=log(10)*ed(i)*(1-tgp/tan(a))*0.5/csc(2*phi*pi/180); %(tgp+1/tgp);
    ms(i)=m1;
    if(m1>m)
    %if(abs(m1-m) < tol)
        im=i-1;
        break;
    end
    
end

alpha=ed(im)
a=ed(im)*pi/2;
tx=1/(tgp/(sin(a)-tgp*cos(a)));
taua=1/(tx*wsp^alpha);

one=ones(1,N);
con=(one+taua*jw.^alpha);

% alpha = 1.1;
% a=2;
% con=(one+(1/a)^alpha*jw.^alpha).*(one+(1/a)^-alpha*jw.^-alpha);

cs=con.*sys;
k=1/abs(cs(Nm));
%con=newk*con;

kp=k
ka=k*taua

% con=(kp+ka*jw.^alpha);
con=k*con;

OP=jw.^(alpha);
cs=sys.*con;




weights=[1:N/2 N/2:-1:1]./(0.5*N); %center
% weights=[1:N/4 N/4:-1:1 zeros(1,N/2)]./(0.5*N); %low freqs
% weights=[zeros(1,N/2) 1:N/4 N/4:-1:1 ]./(0.5*N); %high freqs

warning('off','MATLAB:nearlySingularMatrix')

[Cn, Cd]=invfreqs(con,w,3,4,weights,10);
fPD=minreal(tf(Cn,Cd));

% [Cn, Cd]=invfreqs(OP,w,2,2,weights,10);
% op=minreal(tf(Cn,Cd));
% fPD=kp+ka*op;

warning('on','MATLAB:nearlySingularMatrix')

zpk(fPD)
fPDz=c2d(fPD,dts);

%bode(ol*fPD);

% figure; grid on;
% cbode(con,w);
% bode(fPD);

% figure; grid on;
% cbode(cs,w);
% bode(ol*fPD);



fig=figure;
grid on;
title('Bode OpenLoop FOC Yaw Phase margin: '+spm+' Crossover frequency: '+swsp')

cbode(cs,w);

% cbode(pldata,w);
% margin(fPD*ol);grid on;
% bode(fPD*ol,{wsp/100 wsp*100});grid on;
% ylim([-200 0]);
saveas(fig,'FOC_PhiLoopBode_Y'+swsp+'_'+spm,'epsc');
saveas(fig,'FOC_PhiLoopBode_Y'+swsp+'_'+spm,'jpg');




fig=figure;hold on;
tstep=30/wsp;
dg=0.2;ng=3;
l1=[];
for g=1-dg*ng:dg:1+dg*ng
    %cl=minreal(fPD*g*ol/(fPD*g*ol+1))
    %step(cl);
    step(40*feedback(fPD*g*ol,1));
    l1=[l1; ['loop gain * ' num2str(g,'%.2f')]];
    %step( ( fPD*g*ol/(fPD*g*ol+1) ) )

end
%step(( 1.2*C*ol/(1.2*C*ol*H+1) ),tstep);
%step(( 1.3*C*L/((1.3*C*L+1)*H ) ),tstep);
% step(minreal((L*C/1.1)/((L*C/1.1)+1)  ),tstep);
% step(minreal((L*C/1.2)/((L*C/1.2)+1)  ),tstep);
% step(minreal((L*C/1.3)/((L*C/1.3)+1)  ),tstep);
Leg=legend(l1,'Location','best');grid on;
ylabel("Inclination (deg)",'Interpreter','latex','FontSize',16)
saveas(fig,'FOC_PhitimeResps_Y'+swsp+'_'+spm,'epsc');
saveas(fig,'FOC_PhitimeResps_Y'+swsp+'_'+spm,'jpg');


function y = cbode(cfresp,freq)
    
% Magnitude
m = 20 * log10(abs(cfresp));

% Phase
% phase = mod(angle(cfresp)*180/pi, -360);
phase = angle(cfresp)*180/pi;
% phase = atan2(imag(cfresp),real(cfresp))*180/(2*pi);

Nc = round(size(cfresp,2)/2);
ycenter = mod(angle( cfresp(Nc) )*180/pi, -360);

% Plot
subplot(2,1,1)
semilogx(freq,m);
grid on
ylabel('Magnitude (dB)');

subplot(2,1,2)
semilogx(freq,phase);
grid on
ylabel('Phase (deg)');
xlabel('Frequency (rad/sec)');
% ylim([ycenter-90 ycenter+45])
%yticks([0 0.5 0.8 1])
y=0;

end