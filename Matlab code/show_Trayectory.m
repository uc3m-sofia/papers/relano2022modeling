path=strcat('/Users/humasoft/Documents/GitHub/Soft-Arm/graphs/Vel/Trayectoria/PI/Tumbocho_FOC_1p5_0.csv');

re=csvread(path);
arr=re;
freq=50; %Hz
period=1/freq; %sec
test=length(arr); % Numero de datos por test
time=period:period:test/freq;


% Preallocating for speed
alpha= zeros(1, test); beta= zeros(1, test);
cs1= zeros(1, test); cs2= zeros(1, test);
pitch= zeros(1, test); roll= zeros(1, test); yaw= zeros(1, test);
v_lengths1= zeros(1, test); v_lengths2= zeros(1, test); v_lengths3= zeros(1, test);
pos1= zeros(1, test); pos2= zeros(1, test); pos3= zeros(1, test);
vel1= zeros(1, test); vel2= zeros(1, test); vel3= zeros(1, test);
amp1= zeros(1, test); amp2= zeros(1, test); amp3= zeros(1, test);

for i=1:test %Se asigna el valor de los datos a las variables
    alpha(i)=arr(i,1);
    beta(i)=arr(i,2);
    cs1(i)=arr(i,3);
    cs2(i)=arr(i,4);
    roll(i)=arr(i,5);
    pitch(i)=arr(i,6);
    yaw(i)=arr(i,7);
    v_lengths1(i)=arr(i,8);
    v_lengths2(i)=arr(i,9);
    v_lengths3(i)=arr(i,10);
    pos1(i)=arr(i,11);
    pos2(i)=arr(i,12);
    pos3(i)=arr(i,13);
    vel1(i)=arr(i,14);
    vel2(i)=arr(i,15);
    vel3(i)=arr(i,16);
    amp1(i)=arr(i,17);
    amp2(i)=arr(i,18);
    amp3(i)=arr(i,19);
end

path=strcat('/Users/humasoft/Documents/GitHub/Soft-Arm/graphs/Vel/Trayectoria/PI/Tumbocho_PI_1p5_0.csv');


re=csvread(path);
arr=re;
freq=50; %Hz
period=1/freq; %sec
test=length(arr); % Numero de datos por test
time=period:period:test/freq;



for i=1:test %Se asigna el valor de los datos a las variables
    roll2(i)=arr(i,5);
    pitch2(i)=arr(i,6);
    yaw2(i)=arr(i,7);

end


fig=figure; hold on;
n=4;
d=30;
i=0;
for t=0:0.02:2*pi*n
    i=i+1;
    nt=t/n;
    x_n(i)=(d*sqrt(2)*cos(nt))/(2*(sin(nt)^2+1));
    y_n(i)=(d*sqrt(2)*cos(nt)*sin(nt))/(sin(nt)^2+1);
    
    plot(x_n(i),y_n(i),'r*');
    plot(yaw(i)*180/pi,pitch(i)*180/pi,'b*')

    plot(yaw2(i)*180/pi,pitch2(i)*180/pi,'g*')

    pause(0.01)
end
    


grid on;
title('Massless trajectory | Phase margin: 60 Crossover frequency: 5')
legend({'Reference','Output FOC','Output PI'},'Location','southeast')
xlabel('Yaw (degree)');
ylabel('Pitch (degree)');

%saveas(fig,'Tumbocho5','epsc');
%saveas(fig,'Tumbocho5','pdf');
