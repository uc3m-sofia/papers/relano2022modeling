e-cienciaDatos: "readme" form 



-------------------

GENERAL INFORMATION

-------------------

1. Title of dataset


SoftArmControl: Data and results of the identification and control of a soft robotic arm.

https://doi.org/10.21950/J95IFS

2. Contact


   Principal Investigator Contact Information

        Name: Concepción Alicia Monje Micharet
        Institution: University Carlos III of Madrid
        Email: cmonje@ing.uc3m.es
        ORCID: 0000-0001-8295-127X


3. Description of the project


Perform the soft robotic arm model identification and design PI and fractional order controllers for a robust performance of the arm. With these controllers, the soft arm performance is analyzed experimentally.


4. Description of the dataset


Different csv files are available that include information about the pose (yaw and pitch angles) of the soft arm while varying the position of its motors. Three datasets are specifically available: experimental data used for the identification of the soft arm model; experimental data from the use of different controllers for the soft arm to reach a pose; and experimental data from the use of different controllers for the soft arm to follow a trajectory.


Apart from the datasets, different codes have been also generated in Matlab and C++ to manage the datasets and obtain the results shown in the associated paper, mainly the following results: soft arm model identification results; control results when using a PI controller for the robot to reach a pose and follow a trajectory; control results when using a fractional-order controller for the robot to reach a pose and follow a trajectory.


5. Notes


The description of the soft robotic arm where the experiments are performed, the method for identifying the soft arm model through experimental data, the design and implementation of the controllers proposed and the description of the trajectories to test the arm performance can be found in:

Relaño, C., Muñoz, J., Monje, C. A., Martínez, S., & González, D. (2022). Modeling and Control of a Soft Robotic Arm Based on a Fractional Order Control Approach. Fractal and Fractional, 7(1), 8. https://doi.org/10.3390/fractalfract7010008




6. Deposit date





7. Date





8. Language

English



--------------------------

AUTHOR INFORMATION

--------------------------

1. Author



        Name: Carlos 
        Last name: Relaño
        Institution: University Carlos III de Madrid
        Email: crelano@ing.uc3m.es
	ORCID: https://orcid.org/0000-0002-3276-4668

        Name: Jorge
        Last name: Muñoz
        Institution: CSIC
        Email: jmyanezb@ing.uc3m.es 
	ORCID: https://orcid.org/0000-0003-1820-1831

        Name: Concepción A.
        Last name: Monje
        Institution: University Carlos III de Madrid
        Email: cmonje@ing.uc3m.es
	ORCID: https://orcid.org/0000-0001-8295-127X

        Name: Santiago
        Last name: Martínez de la Casa
        Institution: University Carlos III de Madrid
        Email: scasa@ing.uc3m.es
	ORCID: https://orcid.org/0000-0003-3539-4583

        Name: Daniel
        Last name: González
        Institution: Arquimea
        Email: dgonzalez@arquimea.com 
	ORCID: 


--------------------------

METHODOLOGY

--------------------------

1. Methodology


- To obtain the soft arm model identification dataset, different input actions are performed on the arm in open loop and the results of the soft arm outputs are saved as described in "main_vel_identification.cpp". The resulting identification datasets are included in "Identification_data.zip". 

- To obtain the soft arm model and design the PI and fractional-order controllers using the dataset "Identificaction_data.zip", the following Matlab files are used: "ident_plant.m" for model identification; "ident_controller_PID.m" for PI controller design; "isomFOCy.m" and "isomFOCp.m" for fractional-oder controller design for yaw and pitch angles, respectively. 

- To analyze the behavior of the soft arm with the controllers designed, real experiments are performed with the soft arm using the following C++ code: "main_vel_controller.cpp" to collect data from PI control; "main_vel_focontroller.cpp" to collect data from fractional-oder control; "vel_trajectory.cpp" to collect data of the robot when tracking/following a trajectory. The resulting data are stored in datasets "Control_data.zip" and "Trayectory_data.zip".

- To visualize the dataset results in Matlab, the following files are used: "Plot_vel_control.m" for plotting control charts; "show_control_velocidad_data.m" for plotting control results; "read_control_velocidad_data.m" for reading control results; "show_Trayectory.m" for plotting trajectory data.




2. Software


Matlab 

Any C++ IDE (such as QtCreator)

 

--------------------------

KEYWORDS

--------------------------

1. Keywords



soft robotics; fractional order control; modeling of soft robots; control of soft robots



--------------------------

SPONSORSHIP INFORMATION AND GRANT IDs

--------------------------

1. Grant Information



This research has been supported by the project SOFIA, with reference PID2020-113194GB-I00, funded by the Spanish Ministry of Economics, Industry and Competitiveness (MCIN/ AEI /10.13039/501100011033), and the project Desarrollo de articulaciones blandas para aplicaciones robóticas, with reference IND2020/IND-1739, funded by the Community of Madrid (CAM) (Department of Education and Research).


--------------------------

RELATED PUBLICATIONS

--------------------------

1. Related publication


Relaño, C., Muñoz, J., Monje, C. A., Martínez, S., & González, D. (2022). Modeling and Control of a Soft Robotic Arm Based on a Fractional Order Control Approach. Fractal and Fractional, 7(1), 8.
https://doi.org/10.3390/fractalfract7010008




2. Related dataset





--------------------------

GEOGRAPHIC INFORMATION

--------------------------

1. Spatial coverage





--------------------------

TEMPORAL INFORMATION

--------------------------

1. Time period coverage







--------------------------

FILES

--------------------------

1. Files 

"Code" zipped folder containing:

"Data" folder containing:

Identification_data.zip = Compressed file containing the csv files of the soft arm data to perform the identification. These are collected at different time intervals and with different input patterns (more information can be found in the code used for identification: main_vel_identification.cpp).

Control_data.zip = Compressed file containing the csv files of the soft robot data using the different controllers and used to represent the graphs of the article. These are collected in different positions and with different input values (more information can be found in the code used for the soft arm control: main_vel_focontroller.cpp and main_vel_controller.cpp).

Trayectory_data.zip = Compressed file containing the csv files of the data of the soft arm using the different trajectories and used to represent the graphs of the article. These are collected at different values of the trajectory (more information can be found in the code used for the trajectories tracking: vel_trayectoria.cpp).





"Matlab code" folder containing

ident_plant.m =  Matlab file to create the identifications through the dataset of the arm model identification.

isomFOCy.m = Matlab file for the design of the fractional-order controller for the yaw movement of the arm.

isomFOCp.m = Matlab file for the design of the fractional-order controller for the pitch movement of the arm.

ident_controller_PID.m = Matlab file for the identifications and the design of the PI controller.

Plot_vel_control.m =  Matlab file for plotting control charts (using show_control_velocidad_data.m and read_control_velocidad_data.m)

show_control_velocidad_data.m = Matlab function for plotting the control results.

read_control_velocidad_data.m = Matlab function for reading the control results.

show_Trayectory.m = Matlab file for plotting the trajectory data.






"Robot code" (C++ code) folder containing

main_vel_identification.cpp = C++ file to collect data from the soft arm for identification.

main_vel_focontroller.cpp = C++ file to collect soft arm data using a fractional-order controller.

main_vel_controller.cpp = C++ file to collect soft arm data using a PI controller.

vel_trayectoria.cpp = C++ file to collect data from the soft arm when following a trajectory.


SoftArmControl_2023_v01_readme.txt = documentation

--------------------------

LICENSES AND PRIVACITY

--------------------------

1. Licenses



Creative Commons CC-BY-NC-ND


2. Privacity





--------------------------

OTHERS

--------------------------

1. Data dictionary











